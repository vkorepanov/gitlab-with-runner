# Gitlab with gitlab-runner

This repo contains files to deploy gitlab with gitlab-runner instance using
docker-compose.

## Prerequisites

Install docker and docker-compose.

Create /media/data/gitlab folder for gitlab configuration, logs and data.

Ensure 22 and 4480 ports are free.

## Run

`docker-compose up -d`

Don't use `-d` option if you don't want to run in detached mode.

## Configuration

Gitlab configuration supposed to exists in /media/data/gitlab/config directory.
Otherwise it will be generated on startup by gitlab image.

Gitlab will forward 80 port to 4480 port on the host. And 22 to 22. To change
this behavior - check ports section of docker-compose.yml.

Runner configuration uses HTTP access to gitlab. Runner will starting jobs in
docker with ubuntu 19.10 image by default.
